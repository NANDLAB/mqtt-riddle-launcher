# MQTT Riddle Launcher
## Dependencies
```
sudo apt install jq mosquitto-clients
```
## Autostart
```
mkdir ~/.config/autostart && ln -s -t ~/.config/autostart ~/git/mqtt-riddle-launcher/mqtt-riddle-launcher.desktop
```
