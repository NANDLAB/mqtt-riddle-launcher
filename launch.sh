#!/usr/bin/sh

riddles_kill() {
	cat "$HOME/git/mqtt-riddle-launcher/riddles.sh" | while read -r riddle ; do
		/usr/bin/pkill -f "$riddle"
	done
}

riddles_start() {
	cat "$HOME/git/mqtt-riddle-launcher/riddles.sh" | while read -r riddle ; do
		/usr/bin/sh -c "$riddle" &
	done
}

main() {
	trap "riddles_kill ; exit" 1 2 3 9 15

	if [ ! -e "$HOME/git/mqtt-riddle-launcher/riddles.sh" ] ; then
		echo "riddles.sh file does not exist!"
		exit 1
	fi

	broker=$(jq -r .hostname ~/.mqtt.json)
	if [ -n "$broker" ] ; then
		echo "Broker hostname $broker was read from ~/.mqtt.json ."
	else
		broker="localhost"
		echo "Cannot read ~/.mqtt.json , using localhost as broker."
	fi

	echo "Launching all riddles..."
	riddles_kill
	sleep 1
	riddles_start

	echo "Listening for reset signal..."
	while true ; do
		/usr/bin/mosquitto_sub -L "mqtt://${broker}/reset" | while read -r line ; do
			echo "Reset signal received. Restarting all riddles..."
			riddles_kill
			sleep 1
			riddles_start
		done
		sleep 2
	done
}

main
